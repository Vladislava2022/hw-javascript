
window.addEventListener("DOMContentLoaded", () => {
    const btn = document.querySelectorAll(".keys input"), // всі кнопки
        display = document.querySelector(".display > input"), // екран калькулятора
        reset = document.querySelector(".reset"); // кнопка С

    //створення об'єкту
    const calc = {
        value1: "",
        value2: "",
        oper: "",
        rez: "",
        memory: "",
    }

    btn.forEach((target) => {
        target.addEventListener("click", function (e) {

            let key = e.target.defaultValue; // отримання інформації про ініціатора події

            if (key === "+" || key === "-" || key === "*" || key === "/" || key === "mrs" || key === "m-" || key === "m+") {
                calc.oper = key; //отримання оператора
                show(calc.oper, display);
            }
            //отримання першого числа
            else if (calc.value2 === "" && calc.oper === "") {
                calc.value1 += key;
                show(calc.value1, display) // вивід першого числа на екран 
            }
            //отримання другого числа
            else if (calc.oper !== "" && key !== "=" && calc.rez === "") {
                calc.value2 += key;
                show(calc.value2, display);
            }
            else if (calc.rez !== "" && key !== "=") {
                calc.value1 = calc.rez;
                calc.value2 += key;
                show(calc.value2, display);
            }
            

            //математичні операції
            if (key === "=") {
                switch (calc.oper) {
                    case "+":
                        calc.rez = parseInt(calc.value1) + parseInt(calc.value2);
                        break;
                    case "-":
                        calc.rez = parseInt(calc.value1) - parseInt(calc.value2);
                        break;
                    case "*":
                        calc.rez = parseInt(calc.value1) * parseInt(calc.value2);
                        break;
                    case "/":
                        if (calc.value2 === "0") {
                            calc.value1 = "";
                            calc.value2 = "";
                            calc.oper = "";
                            calc.rez = "";
                            display.value = "error";
                            return;
                        } else if (calc.value2 !== "0") {
                            calc.rez = parseInt(calc.value1) / parseInt(calc.value2);
                            break;
                        }
                }
                show(calc.rez, display);
                calc.value2 = "";
            }

            if (key === "m+" || key === "m-") { // збереження в пам'яті
                calc.memory = calc.rez;
                display.value = "m";
            }
            else if (key === "mrc") { // вивід збереженого в пам'яті результату попередньої операції на екран
                show(calc.memory, display)
            }
            else if (key === reset.value) { //кнопка С - очищення
                calc.value1 = "";
                calc.value2 = "";
                calc.oper = "";
                calc.memory = calc.rez;
                show("", display);
            }

            //функція виводу данних на екран калькулятора
            function show(value, display) {
                display.value = value;
            }
        })
    })
})