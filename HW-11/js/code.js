// завдання №1: Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
//Додати до класу метод для виведення імені та прізвища
class User {
        constructor() {
                this.name = "";
                this.lastname = "";
        }
        show() {
                this.user = document.createElement("div");
                document.body.append(this.user);
                this.user.classList.add("firstDiv");
                this.user.textContent = `Ім'я: ${this.name}, прізвище: ${this.lastname}`;
        }
}
let object1 = new User();
object1.name = "Ivan";
object1.lastname = "Ivanov";
object1.show();


window.addEventListener('DOMContentLoaded', () => {

        //завдання №2: Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і 
        //з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний
        const list = document.createElement('ul');
        document.body.append(list);
        for (let i = 0; i < 4; i++) {
                let listChild = document.createElement('li');
                list.appendChild(listChild);
        }

        let li1 = document.getElementsByTagName("li")[1];
        let li2 = document.getElementsByTagName("li")[2];
        li1.addEventListener('click', () => {
                list.firstElementChild.classList.add("blue");
                li2.classList.add("red");
        })

        //завдання №3: Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
        //При наведенні мишки виведіть текст координати, де знаходиться курсор мишки
        const div = document.createElement("div");
        document.body.append(div);
        div.classList.add("secondDiv");
        div.addEventListener("mouseover", (e) => {
                div.innerText = `Координата мишки по осі Х: ${e.clientX}, координата мишки по осі У: ${e.clientY}`
        })

        //завдання №4: Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута
        let buttons = [];
        for (let i = 0; i < 4; i++) {
                buttons = document.createElement("input");
                document.body.append(buttons);
                buttons.setAttribute("type", "button");
                buttons.setAttribute("value", [i + 1]);
                buttons.style.marginLeft = "10px";

                buttons.addEventListener("click", (e) => {
                        const info = document.createElement("span");
                        buttons.after(info);
                        info.innerText = `Була натиснута кнопка '${e.target.value}'`;
                        info.style.marginLeft = "10px";
                })
        }

        //завдання №5: Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці
        const thirdDiv = document.createElement("div");
        document.body.append(thirdDiv);
        thirdDiv.classList.add("thirdDiv");
        thirdDiv.addEventListener("mouseover", () => {
                thirdDiv.style.left = `${Math.random() * -500}px`;
                thirdDiv.style.top = `${Math.random() * -200}px`;
        })

        //завдання №6: Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body
        const color = document.createElement("input");
        document.body.append(color);
        color.setAttribute("type", "color");
        color.classList.add("color");
        color.addEventListener("input", (e) => {
                let userChoice = e.target.value;
                document.body.style.backgroundColor = userChoice;
        })

        //завдання №7: Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль
        const inputLogin = document.createElement("input");
        document.body.append(inputLogin);
        inputLogin.setAttribute("placeholder", "Login");
        inputLogin.style.display = "block";
        inputLogin.classList.add("inputLogin");
        inputLogin.addEventListener("input", () => {
                console.dir(inputLogin.value)
        })

        //завдання №8: Створіть поле для введення даних у полі введення даних виведіть текст під полем
        const inputInfo = document.createElement("input");
        document.body.append(inputInfo);
        inputInfo.setAttribute("placeholder", "Enter info");
        inputInfo.style.display = "block";
        inputInfo.classList.add("inputInfo");
        inputInfo.addEventListener("change", () => {
                inputInfo.after(inputInfo.value);
        })

})


