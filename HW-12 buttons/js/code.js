document.body.addEventListener("keypress", (e) => {
        const enterBtn = document.querySelector(".enterBtn"),
                sBtn = document.querySelector(".sBtn"),
                eBtn = document.querySelector(".eBtn"),
                oBtn = document.querySelector(".oBtn"),
                nBtn = document.querySelector(".nBtn"),
                lBtn = document.querySelector(".lBtn"),
                zBtn = document.querySelector(".zBtn"),
                buttons = document.querySelectorAll(".btn");

        switch (e.code) {
                case "Enter":
                        clear(buttons);
                        enterBtn.classList.add("blueBtn");
                        break;
                case "KeyS":
                        clear(buttons);
                        sBtn.classList.add("blueBtn");
                        break;
                case "KeyE":
                        clear(buttons);
                        eBtn.classList.add("blueBtn");
                        break;
                case "KeyO":
                        clear(buttons);
                        oBtn.classList.add("blueBtn");
                        break;
                case "KeyN":
                        clear(buttons);
                        nBtn.classList.add("blueBtn");
                        break;
                case "KeyL":
                        clear(buttons);
                        lBtn.classList.add("blueBtn");
                        break;
                case "KeyZ":
                        clear(buttons);
                        zBtn.classList.add("blueBtn");
                        break;
        }

        function clear(allBtn) {
                allBtn.forEach(element => {
                        element.classList.add("clearBtn");
                        element.classList.remove("blueBtn");
                })
        }
})