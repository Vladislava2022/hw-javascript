/*
Задачі:
1) При натиску кнопок "маленька", "середня", "велика" - нижче висвічується та відповідно змінюється ціна піцци;

2) Реалізувати перетягування соусів та топінгів на корж піцци, 
при цьому нижче має висвідчуватись, які соуси/топінги користувач обрав;

3) Отримати дані, які користувач вводить в поля "імя", "номер телефону", "електронна пошта";

4) При події наведення мишки на кнопку "отримання знижки" ця кнопка "тікає" від користувача.
*/


window.addEventListener("load", function () {
        // задача 1
        let pizzaSize = document.querySelectorAll(".radioIn");

        let sausesCost = 0,
                toppingCost = 0,
                pizzaCost = 200;

        let price = document.querySelector(".price"),
                cost = document.createElement("div");

        cost.classList.add("infoStyle");
        price.after(cost);

        // виводимо кінцеву вартість
        function totalCost() {
                cost.innerText = `${pizzaCost + sausesCost + toppingCost} грн.`
        }

        pizzaSize.forEach(element => {
                element.addEventListener("click", (e) => {

                        // перевіряємо, яка кнопка була натиснута, і відповідно визначаємо ціну залежно від обраного розміру піцци
                        switch (e.target.value) {
                                case "small": {
                                        pizzaCost = 80;
                                        break;
                                }
                                case "mid": {
                                        pizzaCost = 150;
                                        break;
                                }
                                case "big": {
                                        pizzaCost = 200;
                                        break;
                                }
                        }
                        totalCost();
                })
        })

        // задача 2
        let target = document.querySelector(".table");

        // створюємо масив з зображень
        let [...dragIngr] = document.querySelectorAll(".draggable");

        // перебираємо масив зображень та повертаємо кожен елемент в змінну ingr 
        let ingr = dragIngr.map(element => {
                return element
        })

        // перебираємо масив елементів ingr та на кожен елемент вішаємо подію dragstart
        ingr.forEach(function (element) {
                element.addEventListener("dragstart", function (el) {
                        el.dataTransfer.effectAllowed = "move";
                        el.dataTransfer.setData("Text", this.id);
                }, false)
        })

        // відміняємо стандарний обробник події dragover
        target.addEventListener("dragover", function (e) {
                if (e.preventDefault) e.preventDefault();
                return false;
        }, false)

        target.addEventListener("drop", function (event) {
                if (event.preventDefault) event.preventDefault; // відміняємо стандарний обробник події
                if (event.stopPropagation) event.stopPropagation; // зупиняємо розповсюдження події по DOM

                let id = event.dataTransfer.getData("Text");
                let element = document.getElementById(id);
                this.append(element);



                //відображення обраних соусів та топінгів, та їх ціни
                let [...targetSauseAll] = document.querySelectorAll(".targetSause");

                let saucesP = document.querySelector(".sauces > p"),
                        topingsP = document.querySelector(".topings > p");

                let saucesShow = document.createElement("div");
                let topingsShow = document.createElement("div");

                saucesShow.classList.add("infoStyle");
                topingsShow.classList.add("infoStyle");

                saucesP.after(saucesShow);
                topingsP.after(topingsShow);

                // отримання дівів - первісного місцезнаходження зображень
                let targetSause = targetSauseAll.map(target => {
                        return target;
                })

                // отримання спанів з назвами інгрідієнтів
                let targetSpan = targetSauseAll.map(target => {
                        return target;
                }).filter(element => {
                        return element.type != "img"
                })
                // вивід інфо про обрані соуси
                function sauceShow(e) {
                        saucesShow.innerText = e;
                }
                // вивід інфо про обрані топінги
                function toppingShow(e) {
                        topingsShow.innerText = e;
                }

                switch (event.target.nextElementSibling.id) {
                        case "sauceClassic": {
                                let s1 = document.createElement("img");
                                targetSause[0].prepend(s1);
                                s1.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                sausesCost = sausesCost + 20;
                                sauceShow(targetSpan[0].innerText);
                                break;
                        }
                        case "sauceBBQ": {
                                let s2 = document.createElement("img");
                                targetSause[1].prepend(s2);
                                s2.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                sausesCost = sausesCost + 20;
                                sauceShow(targetSpan[1].innerText);
                                break;
                        }
                        case "sauceRikotta": {
                                let s3 = document.createElement("img");
                                targetSause[2].prepend(s3);
                                s3.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                sausesCost = sausesCost + 20;
                                sauceShow(targetSpan[2].innerText);
                                break;
                        }
                        case "moc1": {
                                let t1 = document.createElement("img");
                                targetSause[3].prepend(t1);
                                t1.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[3].innerText);
                                break;
                        }
                        case "moc2": {
                                let t2 = document.createElement("img");
                                targetSause[4].prepend(t2);
                                t2.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[4].innerText);
                                break;
                        }
                        case "moc3": {
                                let t3 = document.createElement("img");
                                targetSause[5].prepend(t3);
                                t3.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[5].innerText);
                                break;
                        }
                        case "telya": {
                                let t4 = document.createElement("img");
                                targetSause[6].prepend(t4);
                                t4.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[6].innerText);
                                break;
                        }
                        case "vetch1": {
                                let t5 = document.createElement("img");
                                targetSause[7].prepend(t5);
                                t5.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[7].innerText);
                                break;
                        }
                        case "vetch2": {
                                let t6 = document.createElement("img");
                                targetSause[8].prepend(t6);
                                t6.setAttribute("src", event.target.nextElementSibling.currentSrc);
                                toppingCost = toppingCost + 50;
                                toppingShow(targetSpan[8].innerText);
                                break;
                        }
                }
                totalCost();

                // видалення інгрідієнтів на піцці по кліку
                element.addEventListener("click", (e) => {
                        let targetId = e.target.id;
                        e.target.remove()

                        function sauceChange() {
                                sauceShow("");
                                sausesCost = sausesCost - 20;
                                totalCost()
                        }

                        function toppingChange() {
                                toppingShow("");
                                toppingCost = toppingCost - 50;
                                totalCost()
                        }

                        switch (targetId) {
                                case "sauceClassic": {
                                        sauceChange()
                                        break;
                                }
                                case "sauceBBQ": {
                                        sauceChange()
                                        break;
                                }
                                case "sauceRikotta": {
                                        sauceChange()
                                        break;
                                }
                                case "moc1": {
                                        toppingChange()
                                        break;
                                }
                                case "moc2": {
                                        toppingChange()
                                        break;
                                }
                                case "moc3": {
                                        toppingChange()
                                        break;
                                }
                                case "telya": {
                                        toppingChange()
                                        break;
                                }
                                case "vetch1": {
                                        toppingChange()
                                        break;
                                }
                                case "vetch2": {
                                        toppingChange()
                                        break;
                                }
                        }
                })
        }, false)

        // задача 3
        localStorage.user = JSON.stringify([]);

        let [...allInputs] = document.querySelectorAll(".formInput");

        let inputs = allInputs.map(element => {
                return element; // повертаємо кожний інпут і записуємо в змінну inputs
        })

        class User {
                constructor(name, phone, email) {
                        this.name = name;
                        this.phone = phone;
                        this.email = email;
                }
        }

        function validate(target) {
                switch (target.name) {
                        case "name": return /^[A-zА-я]{2,}$/i.test(target.value);
                        case "phone": return /^\+380\d{9}$/.test(target.value);
                        case "email": return /^[A-z0-9]{1,}@[A-z]{1,}.com$/.test(target.value);
                        default: throw new Error("Error");
                }
        }
        // на всі елементи масиву inputs вішаємо подію change та передаємо їх в функцію перевірки validate
        inputs.forEach(el => {
                el.addEventListener("change", (event) => {
                        validate(event.target)
                })
        })

        let btnSubmit = document.querySelector("[type=button]");
        // на подію click перебираємо перевірені елементи масиву
        btnSubmit.addEventListener("click", (e) => {
                let validateRez = inputs.map(element => {
                        return validate(element)
                })

                // перевіряємо, чи є помилка (false)
                if (!validateRez.includes(false)) {
                        // якщо помилки в введених данних немає, то дані записуємо в localStorage
                        let userInfo = JSON.parse(localStorage.user)
                        userInfo.push(new User(...inputs.map(element => {
                                return element.value
                        })))
                        localStorage.user = JSON.stringify(userInfo);
                        e.target.value = "Замовлення підтверджено!"
                        e.target.style.background = "green"
                } else if (validateRez.includes(false)) {
                        e.target.style.background = "red";
                        e.target.value = "Перевірте Ваші данні і натисність ще раз"
                }
        })

        // реалізуємо функцію очищення даних в формі по кнопці reset
        let reset = document.querySelector("[type=reset]");
        reset.addEventListener("click", () => {
                inputs.forEach(element => element.value = "");
                let a = document.querySelector("[type=button]")
                a.value = "Підтвердити замовлення >>"
        })

        // задача 4
        let banner = document.getElementById("banner");

        banner.addEventListener("mouseover", () => {
                banner.style.left = `${Math.random() * 1020}px`;
                banner.style.top = `${Math.random() * 500}px`;
                banner.style.maxWidth = 250 + "px";
                banner.style.maxHeight = 106 + "px";
        })
})