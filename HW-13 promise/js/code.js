/*
Выполнить запрос на https://swapi.dev/api/people получить список героев звездных войн.

Вывести каждого героя отдельной карточкой с указанием имени, половой принадлежности,
года рождения и планеты, на которой родился.
            
Создайте кнопку сохранить на каждой карточке. При нажатии кнопки запишите информацию в браузере.

Для создания карточек используйте классы.
*/

const url = "http://swapi.dev/api/people";

const data = fetch(url, { method: "get" });

let dataInfo = data.then((rez) => rez.json(), (error) => console.error(error));

dataInfo.then((arg) => show(arg.results));

function show(arr) {
        arr.forEach(element => {
                // деструктурізація, отримання даних про персонажів
                const { name, gender, birth_year, homeworld } = element;

                //Отримання планет за посиланнями в homeworld, запис планет в константу planets для подальшого відображення в картках
                const planetLink = fetch(homeworld, { method: "get" });
                let planetInfo = planetLink.then((rez) => rez.json(), (error) => console.error(error));

                planetInfo.then((arrayPlanet) => getPlanets(arrayPlanet.name));

                function getPlanets(allPlanets) {
                        const planets = allPlanets;

                        //створення основної картки персонажа
                        let card = document.createElement("div");
                        document.querySelector(".main").appendChild(card);
                        card.classList.add("mainCards");

                        //створення класів для додавання в картку інформації про імя, гендер, д.н., планету
                        class NameCard {
                                showName() {
                                        let divName = document.createElement("div");
                                        card.appendChild(divName);
                                        divName.innerText = `Name: ${name}`;
                                        divName.classList.add("divName");
                                }
                        }
                        const el1 = new NameCard(name);
                        el1.showName();

                        class GenderCard {
                                showGender() {
                                        let divGender = document.createElement("div");
                                        card.appendChild(divGender);
                                        divGender.innerText = `Gender: ${gender}`;
                                        divGender.classList.add("divGender");
                                }
                        }

                        const el2 = new GenderCard(gender);
                        el2.showGender();

                        class BirthCard {
                                showBirth() {
                                        let divBirth = document.createElement("div");
                                        card.appendChild(divBirth);
                                        divBirth.innerText = `Birth year: ${birth_year}`;
                                        divBirth.classList.add("divBirth");
                                }
                        }

                        const el3 = new BirthCard(birth_year);
                        el3.showBirth();

                        class PlanetCard {
                                showPlanet() {
                                        let divPlanet = document.createElement("div");
                                        card.appendChild(divPlanet);
                                        divPlanet.innerText = `Homeworld: ${planets}`;
                                        divPlanet.classList.add("divPlanet");
                                }
                        }

                        const el4 = new PlanetCard(planets);
                        el4.showPlanet();

                        //створення об'єкту персонажу
                        let Persons = {
                                name: name,
                                gender: gender,
                                birth_year: birth_year,
                                planets: homeworld,
                        }

                        //створення кнопки "Зберегти"
                        class Button {
                                clickButton() {
                                        let btn = document.createElement("input");
                                        btn.setAttribute("type", "button")
                                        //btn.innerText = "Save";
                                        btn.setAttribute("value", "Save");
                                        card.appendChild(btn);
                                        btn.classList.add("button");

                                }
                        }

                        let buttons = new Button();
                        buttons.clickButton();

                        //запис інформації в браузер по події нажаття на кнопку "Зберегти"
                        document.querySelectorAll(".button").forEach(elements => {
                                elements.addEventListener("click", (e) => {
                                        localStorage.setItem("persons", JSON.stringify(Persons));
                                        e.target.classList.add("saveBtn");
                                        e.target.value = "Saved";
                                });
                        })
                }
        })
}


