//Завдання №1
/* Перетворити за допомогою ?:
if (a + b < 4) {
        result = 'Мало';
} else {
        result = 'Много';
}
*/

let num1 = parseFloat(prompt('Введіть число від 1 до 10'));
let num2 = parseFloat(prompt('Введіть число від 1 до 10'));
let rez = num1 + num2 < 4 ? 'Мало' : 'Много';
document.write("Результат першого завдання: " + rez + "<br>");

//Завдання №2

/*Перетворити за допомогою ?:
var message;
if (login == 'Вася'){
        message = 'Привет';
        } else if (login == 'Директор'){
                message - 'Здравствуйте';
        } else if (login == ''){
                message = 'Нет логина';
        }else {
                message = '';
        }
*/

let login = prompt('Введіть логін');
let message;
message = (login === 'Вася') ? 'Привет' : (login === "Директор") ? 'Здравствуйте' : (login === '') ? 'Нет логина' : '';
document.write(message + "<br>");


//Завдання №3
/*Вивести суму чисел в проміжку між А та В;
  Вивести всі непарні числа в проміжку між А та В.
*/
var a = parseFloat(prompt('Введіть перше число'));
var b = parseFloat(prompt('Введіть друге число, яке буде більшим за перше'));
var sum = 0;

for(let i = a + 1; i < b; i++){
        console.log(sum += i);       
 if (i % 2 !== 0){
        document.write("<br>" + "Непарні числа: " + i);
}
}
document.write("<br>" + "Сума чисел в проміжку між А та В складає : " + sum);

