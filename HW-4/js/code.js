/*
Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает 
каждый элемент массива этой функцией, возвращая новый массив.
*/
function map(fn, array){
        const newArray = [];
        for (let i = 0; i < array.length; i++){
                newArray.push(fn(array[i]));
        }
        return(newArray);
}

console.log(map(x => x * 1, [0, 1, 2, 3, 4, 5]) + '<br>');

/*
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. 
В ином случае она задаёт вопрос confirm и возвращает его результат.
1	function checkAge(age) {
2	if (age > 18) {
3	return true;
4	} else {
5	return confirm('Родители разрешили?');
6	} }
*/

let age = prompt('Введіть свій вік');
function checkAge(age) {
return rez = age > 18 ? true : confirm("Батьки дозволили?");
}
checkAge(age);
console.log(`age: ${age} rez: ${rez}`)