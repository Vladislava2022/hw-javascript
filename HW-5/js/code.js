/*
Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
Создать в объекте вложенный объект - "Приложение". 
Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
Создать методы для заполнения и отображения документа.    
*/

var doc = {
        header: "",
        body: "",
        footer: "",
        date: "",

        fill: function () {
                for (let element in doc) {
                        if (doc[element] < 5) {
                                doc[element] = prompt(`Enter ${element} info`);
                                document.write(`${doc[element]} <br>`);
                        }
        },
       
        app: {
                header: "",
                body: "",
                footer: "",
                date: "",
                appfill: function () {
                        for (let element in doc.app) {
                                if (doc.app[element] < 5) {
                                        doc.app[element] = prompt(`Enter app ${element} info`);
                                        document.write(`${doc.app[element]} <br>`);
                                }
                }
        },
}

doc.fill();
doc.app.appfill();
