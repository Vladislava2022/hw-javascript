/*
№1
Разработайте функцию-конструктор, которая будет создавать объект Human (человек).
Создайте массив объектов и реализуйте функцию, которая будет сортировать элементы 
массива по значению свойства Age по возрастанию или по убыванию.
*/

function Human(age) {
        this.age = age;
}

const humanObjects = [
        new Human(10),
        new Human(120),
        new Human(25),
        new Human(13),
        new Human(5)
];

let objectCopy = humanObjects.concat();

function objectSortOne (humanArray) {
        humanArray.sort((a, b) => a.age - b.age)
};

function objectSortTwo (humanArray) {
        humanArray.sort((a, b) => b.age - a.age)
};

objectSortOne(humanObjects);
objectSortTwo(objectCopy);

console.log(humanObjects);
console.log(objectCopy);