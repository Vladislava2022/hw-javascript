/*
При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг". 
Данная кнопка должна являться единственным контентом в теле HTML документа, 
весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. 
При нажатии на кнопку "Нарисовать" создать на странице 100 кругов (10*10) случайного цвета. 
При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, 
то есть все остальные круги сдвигаются влево.
*/
window.onload = () => {

        const button = document.getElementById("btn");

        button.onclick = () => {
                const diam = document.createElement("input");
                diam.classList.add("diametr");
                button.after(diam);
                diam.setAttribute("type", "text");
                diam.setAttribute("placeholder", "Введіть діаметр кола і натисніть 'Намалювати'");


                const button2 = document.createElement("input");
                document.body.append(button2);
                button2.setAttribute("type", "button");
                button2.setAttribute("value", "Намалювати");


                const box = document.createElement("div");
                button2.after(box);
                box.classList.add("box");


                button2.onclick = () => {

                        const diametr = document.querySelector(".diametr").value;

                        for (let i = 0; i < 100; i++) {
                                const div = document.createElement("div");
                                box.appendChild(div);
                                div.classList.add("div");
                                div.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%`;
                                div.style.width = diametr + `px`;
                                div.style.height = diametr + `px`;

                                div.onclick = () => {
                                        div.style.display = `none`;
                                }
                        }
                }
        }
}