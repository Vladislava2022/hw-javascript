/*
Реалізуйте програму перевірки телефону

Використовуючи JS Створіть поле для введення телефону та кнопку збереження
Користувач повинен ввести номер телефону у форматі 000-000-00-00 

Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний
зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
якщо буде помилка, відобразіть її в діві до input.
*/
window.onload = () => {

const tel = document.createElement("input");
document.body.append(tel);
tel.classList.add("tel");
tel.setAttribute("type", "tel");
tel.setAttribute("placeholder", "Phone number in the format: 000-000-00-00");


const saveBtn = document.createElement("input");
tel.after(saveBtn);
saveBtn.setAttribute("type", "button");
saveBtn.setAttribute("value", "Save");

saveBtn.addEventListener("click", () => {
        const pattern = /^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/;
       
        if (pattern.test(tel.value)) {
                document.body.style.backgroundColor = "green";
                document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
        } else {
                const div = document.createElement("div");
                tel.before(div);
                div.textContent = "Phone format error";
        }
})
}