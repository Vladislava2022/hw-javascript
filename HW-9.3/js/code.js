/*
Слайдер 
            Створіть слайдер, який кожні 3 сек змінюватиме зображення 
            Зображення для відображення
            https://cdn.wallpapersafari.com/41/13/HJkG5Z.jpg
            https://gdb.rferl.org/74AA6BE2-5A47-42EC-98E1-75C2197AAE4E_w1200_r1.jpg
            https://i.ytimg.com/vi/wXQotPuPb0s/maxresdefault.jpg
            https://miridei.com/files/img/c/idei-puteshestvii/ukraine/tonnel_lubvi.jpg
            https://cdn.wallpapersafari.com/71/85/bmZIu1.jpg
*/
const slides = document.querySelectorAll(".slide")
let slider = document.querySelector("#slider");
let index = 0;

const scroll = () => {
        if (index == slides.length - 1) {
                index = 0;
                activeSlide(index);
        } else {
                index++;
                activeSlide(index);
        }
}

const activeSlide = n => {
        for (element of slides) {
                element.classList.remove("active"); 
        };
       slides[n].classList.add("active");
}

setInterval(scroll, 3000);