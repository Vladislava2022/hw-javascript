/*
Створіть програму секундомір.
            
Секундомір матиме 3 кнопки "Старт, Стоп, Скидання" 
При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
Виведення лічильників у форматі ЧЧ: ММ: СС (або ММ: СС: МС)
Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/


window.onload = () => {

        const hoursElement = document.getElementById("hoursElement"),
        minutesElement = document.getElementById("minutesElement"),
        secondsElement = document.getElementById("secondsElement");

        let hours = 00, minutes = 00, seconds = 00, interval;

        const start = document.getElementById("start"), 
        stop = document.getElementById("stop"),
        reset = document.getElementById("reset"),
        background = document.querySelector(".stopwatch-display");
   
        start.addEventListener("click", () => {
                clearInterval(interval);
                interval = setInterval(startTimer, 1000);
                background.classList.add("green");
                background.classList.remove("silver", "red");
        })

        stop.addEventListener("click", () =>{
                clearInterval(interval);
                background.classList.remove("green", "silver");
                background.classList.add("red");
        })

        reset.addEventListener("click", () => {
                clearInterval(interval);
                seconds = 00;
                minutes = 00;
                hours = 00;
                secondsElement.textContent = "00";
                minutesElement.textContent = "00";
                hoursElement.textContent = "00";
                background.classList.remove("green", "red");
                background.classList.add("silver");
        })

        const startTimer = () => {
                seconds++;
                if (seconds < 10) {
                        secondsElement.innerText = "0" + seconds;
                } else if (seconds < 60) {
                        secondsElement.innerText = seconds;
                } else if (seconds > 59){
                        minutes ++;
                        minutesElement.innerText = "0" + minutes;
                        seconds = 00;
                        secondsElement.innerText = "0" + seconds;
                }
                if(minutes > 9){
                        minutesElement.innerText = minutes;
                } else if (minutes > 59){
                        hours++;
                        hoursElement.innerText = "0" + hours;
                        minutes = 00;
                        minutesElement.innerText = "0" + minutes;
                }
        }
}




