import React from "react";

function Table(props) {
        return (
                <table>
                        <tbody>
                                <tr>
                                        
                                        <td><p>Номер валют</p></td>
                                        <td><p>Назва валют</p></td>
                                        <td><p>Ціна валют</p></td>
                                        
                                </tr>
                                <tr>
                                        <td>
                                                {props.data.map((element) => {
                                                        return <p> {element.r030} </p>
                                                })}
                                        </td>
                                        <td>
                                                {props.data.map((element) => {
                                                        return <p> {element.txt} </p>
                                                })}
                                        </td>
                                        <td>
                                                {props.data.map((element) => {
                                                        return <p> {element.rate} </p>
                                                })}
                                        </td>
                                </tr>
                        </tbody>
                </table>
        )
}

export default Table;