import React from "react";
import ReactDOM from "react-dom";


const App = function () {
        return (
                <div>
                        <h2>Дні тижня:</h2>
                        <ul>
                                <li>Понеділок</li>
                                <li>Вівторок</li>
                                <li>Середа</li>
                                <li>Четвер</li>
                                <li>П'ятниця</li>
                                <li>Субота</li>
                                <li>Неділя</li>
                        </ul>
                        <Months></Months>
                        <ZodiacSigns></ZodiacSigns>
                </div>
        )
}

const Months = function () {
        return (
                <div>
                        <h2>Місяці:</h2>
                        <ul>
                                <li>січень</li>
                                <li>лютий</li>
                                <li>березень</li>
                                <li>квітень</li>
                                <li>травень</li>
                                <li>червень</li>
                                <li>липень</li>
                                <li>серпень</li>
                                <li>вересень</li>
                                <li>жовтень</li>
                                <li>листопад</li>
                                <li>грудень</li>
                        </ul>
                </div>
        )
}

const ZodiacSigns = function () {
        return (
                <div>
                        <h2>Знаки зодіаку:</h2>
                        <ul>
                                <li>Овен</li>
                                <li>Телець</li>
                                <li>Близнята</li>
                                <li>Рак</li>
                                <li>Лев</li>
                                <li>Діва</li>
                                <li>Терези</li>
                                <li>Скорпіон</li>
                                <li>Стрілець</li>
                                <li>Козоріг</li>
                                <li>Водолій</li>
                                <li>Риби</li>
                        </ul>
                </div>
        )
}



ReactDOM.render(<App></App>, document.getElementById("root"));